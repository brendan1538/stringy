package stringy

import (
	"testing"
)

func TestTrunc(t *testing.T) {
	testStr := "test Trunc function"
	expected := "test"
	actual := Trunc(testStr, 3)

	if actual != expected {
		t.Errorf("Expected %v\nactual was %v", expected, actual)
	}
}
