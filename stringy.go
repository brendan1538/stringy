package stringy

import (
	"strings"
)

// Trunc will take a string (s) and truncate it at a given index
// @arg s (string)
// @arg index (int)
// @return string
func Trunc(s string, index int) string {
	sSlice := strings.Split(s, "")
	var truncatedStr string

	for i := 0; i <= index; i++ {
		truncatedStr = truncatedStr + sSlice[i]
	}

	return truncatedStr
}
